# Loki stack demo

## 🚀 QuickStart

```sh
make start-loki-stack
```
## Whats included 

- [X] [Session Slides](https://docs.google.com/presentation/d/1lhrpKC3E88R_sb5to33L79doRak-BnemYswHoVIMQuQ)
- [X] k3d start a 2 node cluter 
  ```sh
  # k3d cluster list
  monitor      1/1       1/1      true
  ```
- [X] create `monitor` namespace
- [X] grafana
- [X] install helm charts for loki-stack 
- [X] install prometheus (show/view metrics alongside logs)
- [ ] configure loki datasource
- [X] configure prometheus datasource

## live demo ...

[![asciicast](https://asciinema.org/a/HWdOFzV326b3lZQRwghEgnDnN.svg)](https://asciinema.org/a/HWdOFzV326b3lZQRwghEgnDnN)

Go to http://grafana.k3d.localhost/ `admin`:`password` see `./assets/charts/grafana/dev-mode.yaml` 

### Promehteus

```sh
make start-prom-stack
```

### Cluste stop / start

```sh
make restart
```

### Logging with Loki 

![img](https://i.imgur.com/fQJx7dX.png)
